<?php

declare(strict_types = 1);

namespace Drupal\Tests\linkyreplacer\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\linkyreplacer\LinkyRealmDeterminator;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for realm determination.
 *
 * @group linkyreplacer
 * @coversDefaultClass \Drupal\linkyreplacer\LinkyRealmDeterminator
 */
final class LinkyReplacerRealmDeterminatorUnitTest extends UnitTestCase {

  /**
   * Tests internal URL detection.
   *
   * @param string $assertUrlString
   *   The URL to test.
   * @param bool $expectIsInternal
   *   Whether to expect the URL is considered internal.
   *
   * @dataProvider providerTestIsInternal
   * @covers ::isInternal
   */
  public function testIsInternal(string $assertUrlString, bool $expectIsInternal): void {
    $configFactory = $this->createMock(ConfigFactoryInterface::class);
    $config = $this->createMock(ImmutableConfig::class);
    $configFactory->expects($this->any())
      ->method('get')
      ->with('linkyreplacer.settings')
      ->willReturn($config);

    $config->expects($this->any())
      ->method('get')
      ->with('internal_patterns')
      ->willReturn("internaldomain.com\r\n*.internaldomain.com\r\n");

    $realmDeterminator = new LinkyRealmDeterminator($configFactory);
    $this->assertEquals($expectIsInternal, $realmDeterminator->isInternal($assertUrlString));
  }

  /**
   * Data provider for testIsInternal.
   *
   * @return array
   *   Data for testing.
   */
  public static function providerTestIsInternal(): array {
    $scenarios = [];

    $scenarios['host considered internal match wildcard - with path'] = [
      'http://www.internaldomain.com/home',
      TRUE,
    ];
    $scenarios['host considered internal match wildcard - front slash'] = [
      'http://www.internaldomain.com/',
      TRUE,
    ];
    $scenarios['host considered internal match wildcard - front no slash'] = [
      'http://www.internaldomain.com',
      TRUE,
    ];
    $scenarios['host considered internal no wildcard - with path'] = [
      'http://internaldomain.com/home',
      TRUE,
    ];
    $scenarios['host considered internal no wildcard - front slash'] = [
      'http://internaldomain.com/',
      TRUE,
    ];
    $scenarios['host considered internal no wildcard - front no slash'] = [
      'http://internaldomain.com',
      TRUE,
    ];
    $scenarios['relative path'] = [
      '/home',
      TRUE,
    ];
    $scenarios['relative slash'] = [
      '/',
      TRUE,
    ];
    $scenarios['relative no value'] = [
      '',
      TRUE,
    ];
    $scenarios['host considered external - with path'] = [
      'http://www.external.com/home',
      FALSE,
    ];
    $scenarios['host considered external - front slash'] = [
      'http://www.external.com/',
      FALSE,
    ];
    $scenarios['host considered external - front no slash'] = [
      'http://www.external.com',
      FALSE,
    ];
    $scenarios['email addresses are not internal'] = [
      'mailto:blah@example.com',
      FALSE,
    ];
    $scenarios['telephone numbers are not internal'] = [
      'tel:123123123',
      FALSE,
    ];
    // Make sure alternative casing of tel, such as 'Tel' have the same
    // behaviour as lower.
    $scenarios['email addresses are not internal casing'] = [
      'MaILtO:blah@example.com',
      FALSE,
    ];
    $scenarios['telephone numbers are not internal casing'] = [
      'TeL:123123123',
      FALSE,
    ];

    return $scenarios;
  }

}
