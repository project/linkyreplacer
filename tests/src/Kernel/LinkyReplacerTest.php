<?php

declare(strict_types = 1);

namespace Drupal\Tests\linkyreplacer\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\linky\Entity\Linky;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests Linky Replacer.
 *
 * @group linkyreplacer
 */
class LinkyReplacerTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'path',
    'path_alias',
    'entity_route_context',
    'entity_test',
    'linkyreplacer',
    'linky',
    'dynamic_entity_reference',
    'link',
    'text',
    'filter',
    'field',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('linky');
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('path_alias');
    $this->installConfig(['linkyreplacer']);
    FieldStorageConfig::create([
      'field_name' => 'testfield',
      'type' => 'text_long',
      'entity_type' => 'entity_test',
    ])->save();
    FieldConfig::create([
      'field_name' => 'testfield',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Basic replacement test.
   */
  public function testReplacement(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="http://google.com/">Hello world</a>';
    $entity->save();
    $this->assertEquals('<a href="/admin/content/linky/1">Hello world</a>', $entity->testfield->value);
  }

  /**
   * Basic replacement test.
   */
  public function testMultipleSameHref(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="http://example.com/1">Link 1</a><a href="http://example.com/2">Link 2</a><a href="http://example.com/1">Link 3</a>';
    $entity->save();
    $this->assertEquals('<a href="/admin/content/linky/1">Link 1</a><a href="/admin/content/linky/2">Link 2</a><a href="/admin/content/linky/1">Link 3</a>', $entity->testfield->value);

    $linkys = Linky::loadMultiple();
    $this->assertCount(2, $linkys);
    $this->assertEquals('Link 1 (http://example.com/1)', $linkys[1]->label());
    $this->assertEquals('Link 2 (http://example.com/2)', $linkys[2]->label());
  }

  /**
   * Test URLs considered internal are not converted to Linky.
   */
  public function testUrlInternal(): void {
    $this->config('linkyreplacer.settings')->set('internal_patterns', '*.example.com')->save(TRUE);
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="http://foo.example.com/">Link 1</a>';
    $entity->save();
    $this->assertEquals('<a href="http://foo.example.com/">Link 1</a>', $entity->testfield->value);
    $this->assertCount(0, Linky::loadMultiple());
  }

  /**
   * Test existing Linky entity is re-used.
   */
  public function testLinkyReuse(): void {
    Linky::create([
      'link' => [
        'uri' => 'http://www.example.com/',
        'title' => 'Bleh',
      ],
    ])->save();
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="http://www.example.com/">Link 1</a>';
    $entity->save();
    $this->assertEquals('<a href="/admin/content/linky/1">Link 1</a>', $entity->testfield->value);
    // No new Linkys are created.
    $this->assertCount(1, Linky::loadMultiple());
  }

  /**
   * Tests non existent Managed Link  isnt attempted to be recreated.
   */
  public function testLinkyNonExistent(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="/admin/content/linky/219312">Link 1</a>';
    $entity->save();
    $this->assertCount(0, Linky::loadMultiple());
  }

  /**
   * Test absolute path HREfs.
   */
  public function testAbsolutePath(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="/foo">Link 1</a>';
    $entity->save();

    $linkys = Linky::loadMultiple();
    $this->assertCount(0, $linkys);
    $this->assertEquals('<a href="/foo">Link 1</a>', $entity->testfield->value);

    $this->setSupportsInternal();

    $entity->save();
    $linkys = Linky::loadMultiple();
    $this->assertCount(1, $linkys);
    $this->assertEquals('<a href="/admin/content/linky/1">Link 1</a>', $entity->testfield->value);
  }

  /**
   * Tests a full URL, considered internal, is converted to absolute.
   *
   * With internal on, all parts stripped so just path + query,
   * e.g http://foo.example.com/path?q=1 -> /path?q=1
   */
  public function testInternalUrlToAbsolutePath(): void {
    $this->setSupportsInternal();
    $this->config('linkyreplacer.settings')->set('internal_patterns', '*.example.com')->save(TRUE);

    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="http://foo.example.com/my/path?a=1&b=2">Link 1</a>';
    $entity->save();
    $this->assertEquals('<a href="/admin/content/linky/1">Link 1</a>', $entity->testfield->value);

    $linkys = Linky::loadMultiple();
    $this->assertCount(1, $linkys);
    $this->assertEquals('base:my/path?a=1&b=2', $linkys[1]->link->uri);
  }

  /**
   * Test relative path HREfs.
   *
   * Relative URLS are not converted. Not currently converted since relative
   * need to be converted to absolute path, not currently enough context to
   * generate absolute, especially as the module may action link replacement
   * in entity insertion, where the path is not yet known.
   *
   * Tests no exceptions/errors from using these.
   */
  public function testRelativePath(): void {
    $this->setSupportsInternal();

    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="/foo">Link 1</a>';
    $entity->save();
    $this->assertCount(1, Linky::loadMultiple());
  }

  /**
   * Tests converting mailto: HREF to managed links.
   */
  public function testMailTo(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="mailto:webmaster@example.com">Email me</a>';
    $entity->save();
    $this->assertCount(0, Linky::loadMultiple());

    \Drupal::configFactory()
      ->getEditable('linkyreplacer.settings')
      ->set('email', TRUE)
      ->save(TRUE);

    $entity->save();
    $linkys = Linky::loadMultiple();
    $this->assertCount(1, $linkys);
    $this->assertEquals('mailto:webmaster@example.com', $linkys[1]->link->uri);
  }

  /**
   * Tests converting tel: HREF to managed links.
   */
  public function testTelephone(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="tel:+61800000000">Phone me</a>';
    $entity->save();
    $this->assertCount(0, Linky::loadMultiple());

    \Drupal::configFactory()
      ->getEditable('linkyreplacer.settings')
      ->set('telephone', TRUE)
      ->save(TRUE);

    $entity->save();
    $linkys = Linky::loadMultiple();
    $this->assertCount(1, $linkys);
    $this->assertEquals('tel:+61800000000', $linkys[1]->link->uri);
  }

  /**
   * Tests malformed tel: hrefs don't throw exceptions.
   *
   * @see https://www.drupal.org/project/drupal/issues/2575577
   */
  public function testTelephoneMalformed(): void {
    $entity = EntityTest::create();
    $entity->testfield->value = '<a href="tel:12345">Phone me</a>';
    $entity->save();
    $this->assertCount(0, Linky::loadMultiple());

    \Drupal::configFactory()
      ->getEditable('linkyreplacer.settings')
      ->set('telephone', TRUE)
      ->save(TRUE);

    $entity->save();
    $this->assertCount(0, Linky::loadMultiple());
  }

  /**
   * If a href references a path of existing entity.
   */
  public function testEntity(): void {
    $this->installEntitySchema('user');
    $this->setUpCurrentUser();

    $entity1 = EntityTest::create();
    $entity1->save();

    $entity2 = EntityTest::create();
    $entity2->testfield->value = '<a href="' . $entity1->toUrl()->toString() . '">Link 1</a>';
    $entity2->save();

    $this->assertCount(0, Linky::loadMultiple());

    $this->setSupportsInternal();
    $entity2->save();
    $linkys = Linky::loadMultiple();
    $this->assertCount(1, $linkys);
    $this->assertEquals('entity:entity_test/1', $linkys[1]->link->uri);
  }

  /**
   * If a href references a path alias of existing entity.
   */
  public function testEntityPathAlias(): void {
    $this->installEntitySchema('user');
    $this->setUpCurrentUser();

    $alias = '/hello/world';

    $entity1 = EntityTest::create();
    $entity1->save();
    PathAlias::create([
      'path' => $entity1->toUrl()->toString(),
      'alias' => $alias,
      'langcode' => $entity1->language()->getId(),
    ])->save();

    $entity2 = EntityTest::create();
    $entity2->testfield->value = '<a href="' . $alias . '">Link 1</a><a href="' . $alias . '#foo">Link 2</a>';
    $entity2->save();

    $this->assertCount(0, Linky::loadMultiple());

    $this->setSupportsInternal();
    $entity2->save();

    $entity3 = EntityTest::create();
    $entity3->testfield->value = '<a href="' . $alias . '">Link 3</a><a href="' . $alias . '?language=fr">Link 3</a>';
    $entity3->save();

    $entity4 = EntityTest::create();
    $entity4->testfield->value = '<a href="' . $alias . '">Link 4</a><a href="' . $alias . '?language=es#foo">Link 4</a>';
    $entity4->save();

    $this->assertEquals('<a href="/admin/content/linky/1">Link 1</a><a href="/admin/content/linky/2">Link 2</a>', $entity2->testfield->value);
    $linkys = Linky::loadMultiple();
    $this->assertCount(4, $linkys);
    $this->assertEquals('entity:entity_test/1', $linkys[1]->link->uri);
    $this->assertEquals('entity:entity_test/1#foo', $linkys[2]->link->uri);
    $this->assertEquals('entity:entity_test/1?language=fr', $linkys[3]->link->uri);
    $this->assertEquals('entity:entity_test/1?language=es#foo', $linkys[4]->link->uri);
  }

  /**
   * Turn on internal link replacement support.
   */
  protected function setSupportsInternal(): void {
    \Drupal::configFactory()
      ->getEditable('linkyreplacer.settings')
      ->set('internal', TRUE)
      ->save(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);
    // Add back path_alias.path_processor since its deleted by parent.
    if ($container->hasDefinition('path_alias.path_processor')) {
      $definition = $container->getDefinition('path_alias.path_processor');
      $definition->addTag('path_processor_inbound');
    }
  }

}
