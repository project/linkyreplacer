<?php

declare(strict_types = 1);

namespace Drupal\linkyreplacer;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\entity_route_context\EntityRouteContextRouteHelperInterface;
use Drupal\linky\LinkyInterface;

/**
 * Utility for dealing with Linky entities.
 */
class LinkyEntityUtility implements LinkyEntityUtilityInterface {

  /**
   * Linky storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $linkyStorage;

  /**
   * Service for determining whether a URL is internal or external.
   *
   * @var \Drupal\linkyreplacer\LinkyRealmDeterminatorInterface
   */
  protected $realmDeterminator;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity route context route helper.
   *
   * @var \Drupal\entity_route_context\EntityRouteContextRouteHelperInterface
   */
  protected $entityRouteContextRouteHelper;

  /**
   * LinkyEntityUtility constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\linkyreplacer\LinkyRealmDeterminatorInterface $realmDeterminator
   *   Service for determining whether a URL is internal or external.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\entity_route_context\EntityRouteContextRouteHelperInterface $entityRouteContextRouteHelper
   *   Entity route context route helper.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LinkyRealmDeterminatorInterface $realmDeterminator, ConfigFactoryInterface $configFactory, EntityRouteContextRouteHelperInterface $entityRouteContextRouteHelper) {
    $this->linkyStorage = $entityTypeManager->getStorage('linky');
    $this->realmDeterminator = $realmDeterminator;
    $this->configFactory = $configFactory;
    $this->entityRouteContextRouteHelper = $entityRouteContextRouteHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeHref(string $href): string {
    if (UrlHelper::isExternal($href) && $this->supportsInternal()) {
      // If a full URI is internal-ish, strip the domain.
      try {
        $url = Url::fromUri($href);
        if ($this->realmDeterminator->isInternal($url->toString())) {
          $parts = parse_url($href);
          // Strip domain and protocol, make it relative.
          $href =
            ($parts['path'] ?? '/') .
            (isset($parts['query']) ? ('?' . $parts['query']) : '') .
            (isset($parts['fragment']) ? ('#' . $parts['fragment']) : '');
        }
      }
      catch (\InvalidArgumentException $e) {
      }
    }

    if (!UrlHelper::isExternal($href) && $this->supportsInternal()) {
      // It is a relative URL.
      if (strpos($href, '/') === 0) {
        $url = Url::fromUri('internal:' . $href);
        if ($url->isRouted()) {
          // Media links should be ignored.
          if (isset($url->getRouteParameters()['media'])) {
            throw new \InvalidArgumentException();
          }

          $routeName = $url->getRouteName();
          $entityTypeId = $this->entityRouteContextRouteHelper->getAllRouteNames()[$routeName] ?? NULL;
          $entityId = $url->getRouteParameters()[$entityTypeId] ?? NULL;
          if ($entityTypeId && $entityId) {
            $query = $url->getOption('query') ? sprintf('?%s', http_build_query($url->getOption('query'))) : NULL;
            $fragment = $url->getOption('fragment') ? sprintf('#%s', $url->getOption('fragment')) : NULL;
            $href = sprintf('entity:%s/%s%s%s', $entityTypeId, $entityId, $query, $fragment);
          }
          else {
            $href = 'internal:' . $url->toString();
          }
        }
        else {
          $href = $url->toUriString();
        }
      }
      else {
        throw new \InvalidArgumentException('Relative paths not supported.');
      }
    }

    return $href;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkyByHref(string $href): ?LinkyInterface {
    // Anchor links are relative, never process.
    if (strpos($href, '#') === 0) {
      throw new \InvalidArgumentException();
    }

    $notUrl = FALSE;
    if (strpos($href, 'tel:') === 0) {
      $notUrl = TRUE;
      if (!$this->supportsTelephone() || strlen($href) <= 9) {
        // It is a 'tel:' link so don't process. Length check is to avoid
        // exceptions from parse_url when the number is less than or equal to
        // 5 characters (so 9 with the tel: prefix).
        // @see https://www.drupal.org/project/drupal/issues/2575577
        throw new \InvalidArgumentException();
      }
    }

    if (strpos($href, 'mailto:') === 0) {
      $notUrl = TRUE;
      if (!$this->supportsMailTo()) {
        // It is a 'mailto:' link so don't process.
        throw new \InvalidArgumentException();
      }
    }

    if (!$notUrl && (!$this->supportsInternal() && $this->realmDeterminator->isInternal($href))) {
      throw new \InvalidArgumentException();
    }

    // It is already a link entity.
    if (preg_match('#/admin/content/linky/(?<entity_id>\d+)#', $href, $matches)) {
      if ($linky = $this->getLinkyById((int) $matches['entity_id'])) {
        return $linky;
      }

      // Linkys should not be made into a Linky.
      throw new \InvalidArgumentException();
    }

    if (!UrlHelper::isExternal($href) && !$this->supportsInternal()) {
      // Internal links cannot be Linky.
      // This check must be *after* the linky path check above.
      throw new \InvalidArgumentException();
    }

    $href = $this->normalizeHref($href);
    return $this->getLinkyByUri($href);
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkyByUri(string $uri): ?LinkyInterface {
    $ids = $this->linkyStorage->getQuery()
      ->condition('link.uri', $uri)
      ->accessCheck(FALSE)
      ->execute();
    if ($ids) {
      $id = reset($ids);
      return $this->getLinkyById((int) $id);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createLinky(string $uri, string $title): LinkyInterface {
    if (mb_strlen($uri) > 2048) {
      throw new \InvalidArgumentException('URL is too long');
    }

    $link = $this->linkyStorage->create([
      'link' => [
        'uri' => $uri,
        'title' => mb_substr($title, 0, 255),
      ],
    ]);
    $link->save();
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkyById(int $id): ?LinkyInterface {
    return $this->linkyStorage->load($id);
  }

  /**
   * Whether HREFs considered internal should be converted to managed links.
   *
   * @return bool
   *   Whether HREFs considered internal should be converted to managed links.
   */
  protected function supportsInternal(): bool {
    return !empty($this->configFactory->get('linkyreplacer.settings')->get('internal'));
  }

  /**
   * Whether email links should be converted to managed links.
   *
   * @return bool
   *   Whether email links should be converted to managed links.
   */
  protected function supportsMailTo(): bool {
    return !empty($this->configFactory->get('linkyreplacer.settings')->get('email'));
  }

  /**
   * Whether telephone links should be converted to managed links.
   *
   * @return bool
   *   Whether telephone links should be converted to managed links.
   */
  protected function supportsTelephone(): bool {
    return !empty($this->configFactory->get('linkyreplacer.settings')->get('telephone'));
  }

}
